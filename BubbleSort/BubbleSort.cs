﻿using System;


namespace BubbleSort
{
    class BubbleSort
    {
        private int[] vector;

        public void Cargar()
        {
            Console.WriteLine("===  BubbleSort  ===\n");
            Console.Write("No. de elementos a ordenar: "); // Recolección de los datos introducidos
            string linea;
            linea = Console.ReadLine();
            int cant;
            cant = int.Parse(linea);
            vector = new int[cant];
            for (int f = 0; f < vector.Length; f++) // Contador
            {
                Console.Write("Ingresar el elemento " + (f + 1) + ": ");
                linea = Console.ReadLine();
                vector[f] = int.Parse(linea);
            }
        }

        // Metodo BubbleSort
        public void MBubbleSort()
        {
            int t;
            for (int a = 1; a < vector.Length; a++)
                for (int b = vector.Length - 1; b >= a; b--)
                {
                    if (vector[b - 1] > vector[b])
                    {
                        t = vector[b - 1];
                        vector[b - 1] = vector[b];
                        vector[b] = t;
                    }
                }
        }

        public void Imprimir()
        {
            Console.WriteLine("Elementos ordenados de menor a mayor");
            for (int f = 0; f < vector.Length; f++)
            {
                Console.Write(vector[f] + "  ");
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            BubbleSort cmdBS = new BubbleSort();
            cmdBS.Cargar();
            cmdBS.MBubbleSort();
            cmdBS.Imprimir();
        }
    }
}