﻿using System;

namespace quicksort
{
    class Class
    {
        static void Main()
        {
            int n;
            Console.WriteLine("===  Quick Sort   ===");
            Console.WriteLine("\n");
            Console.Write("No. de elementos a ordenar: ");
            n = Int32.Parse(Console.ReadLine()); // Recolección de los datos introducidos
            fill b = new fill(n);
        }
    }

    // Funcion para llenar de acuerdo al número ingresado por el usuario ^^^^
    class fill
    {
        int h;
        int[] vector;
        public fill(int n)
        {
            h = n;
            vector = new int[h];
            for (int i = 0; i < h; i++)
            {
                Console.Write("Ingresar el elemento {0}: ", i + 1);
                vector[i] = Int32.Parse(Console.ReadLine());
            }
            quicksort(vector, 0, h - 1);
            mostrar();
        }

        // Metodo Quicksort
        private void quicksort(int[] vector, int primero, int ultimo)
        {
            int i, j, central;
            double pivote;
            central = (primero + ultimo) / 2;
            pivote = vector[central];
            i = primero;
            j = ultimo;
            do
            {
                while (vector[i] < pivote) i++;
                while (vector[j] > pivote) j--;
                if (i <= j)
                {
                    int temp;
                    temp = vector[i];
                    vector[i] = vector[j];
                    vector[j] = temp;
                    i++;
                    j--;
                }
            } while (i <= j);

            if (primero < j)
            {
                quicksort(vector, primero, j);
            }
            if (i < ultimo)
            {
                quicksort(vector, i, ultimo);
            }
        }


        // Funcion para mostrar los elementos ordenados
        private void mostrar()
        {
            Console.WriteLine("\nElementos ordenados de menor a mayor: ");
            for (int i = 0; i < h; i++)
            {
                Console.Write("{0} ", vector[i]);
            }
            Console.ReadLine();
        }
    }
}