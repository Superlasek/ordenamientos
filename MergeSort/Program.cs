﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums = new int[40];
            Console.WriteLine("===  Merge Sort  ===\n\n");
            Console.Write("No. de elementos a ordenar: ");
            string linea;
            linea = Console.ReadLine();
            int cant;
            cant = int.Parse(linea);
            nums = new int[cant];
            for (int f = 0; f < nums.Length; f++)
            {
                Console.Write("Ingresar el elemento " + (f + 1) + ": ");
                linea = Console.ReadLine();
                nums[f] = int.Parse(linea);
            }
            MergeSort(nums);
            Console.WriteLine("Vector Ordenado de menor a mayor: ");
            for (int i = 0; i < nums.Length; i++)
                Console.Write(nums[i] + " ");
            Console.ReadLine();
        }

        public static void MergeSort(int[] x)
        {
            MergeSort(x, 0, x.Length - 1);
        }

        static private void MergeSort(int[] x, int desde, int hasta)
        {
            // Stop
            if (desde == hasta)
                return;

            // Calcular mitad del array
            int mitad = (desde + hasta) / 2;

            MergeSort(x, desde, mitad);
            MergeSort(x, mitad + 1, hasta);

            int[] aux = Merge(x, desde, mitad, mitad + 1, hasta);
            Array.Copy(aux, 0, x, desde, aux.Length);
        }

        // Función que mezcla las dos mitades
        static private int[] Merge(int[] x, int desde1, int hasta1, int desde2, int hasta2)
        {
            int a = desde1;
            int b = desde2;
            int[] result = new int[hasta1 - desde1 + hasta2 - desde2 + 2];

            for (int i = 0; i < result.Length; i++)
            {
                if (b != x.Length)
                {
                    if (a > hasta1 && b <= hasta2)
                    {
                        result[i] = x[b];
                        b++;
                    }
                    if (b > hasta2 && a <= hasta1)
                    {
                        result[i] = x[a];
                        a++;
                    }
                    if (a <= hasta1 && b <= hasta2)
                    {
                        if (x[b] <= x[a])
                        {
                            result[i] = x[b];
                            b++;
                        }
                        else
                        {
                            result[i] = x[a];
                            a++;
                        }
                    }
                }
                else
                {
                    if (a <= hasta1)
                    {
                        result[i] = x[a];
                        a++;
                    }
                }
            }
            return result;
        }
    }
}